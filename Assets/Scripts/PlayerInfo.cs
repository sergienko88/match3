﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerInfo  {

    static int _gold = 0;

    static List<Spell> _spells = new List<Spell>
    {
        new Spell {Id =1,Count=2, Name ="spell1" },
        new Spell { Id =2 , Count =0 , Name ="spell2" }
    };

    public static List<Spell> Spells
    {
        get
        {
            return _spells;
        }
    }

    static void LoadInfo()
    {

    }

    static void SaveInfo()
    {

    }
}
