﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
[Serializable]
public class DimentionAddress2
{
    [SerializeField]
    int _x = 0;
    [SerializeField]
    int _y = 0;

    public int X
    {
        get
        {
            return _x;
        }

        set
        {
            _x = value;
        }
    }

    public int Y
    {
        get
        {
            return _y;
        }

        set
        {
            _y = value;
        }
    }

    public override string ToString()
    {
        return "["+_x+","+_y+"]";
    }
}
[Serializable]
public class Properties {

    [SerializeField]
    DimentionAddress2 _address = new DimentionAddress2();
    [SerializeField]
    int _type = -2;
    public Action PropertyUpdated;

    public bool IsEmpty { get; set; } = false;

    public int Type
    {
        get
        {
            return _type;
        }

        set
        {
            _type = value;
            PropertyUpdated?.Invoke();
        }
    }

    public DimentionAddress2 Address
    {
        get
        {
            return _address;
        }

        set
        {
            _address = value;
            PropertyUpdated?.Invoke();
        }
    }

    public int Points
    {
        get
        {            
            switch (_type)
            {
                case 1: return 1; break;
                case 2: return 2; break;
                case 3: return 3; break;
                case 4: return 4; break;
                case 5: return 5; break;
                default:break;
            }
            return 0;
        }
    }
}

public class UICell : MonoBehaviour,IPointerDownHandler {
    Image _image;
    RectTransform _rectTransform;
    public Action <UICell> OnSelectAction;
    Properties _properties = new Properties();

    Color _defaultColor = Color.white;
    Color _defaultColorEmpty = Color.white;
    public Color DefaultColor
    {
        get
        {
            return _defaultColor;
        }
    }

    public Color DefaultColorEmpty
    {
        get
        {
            return _defaultColorEmpty;
        }
    }


    public RectTransform RectTransform
    {
        get
        {
            return _rectTransform;
        }
    }

    public Image Image
    {
        get
        {
            return _image;
        }
    }

    public Properties Properties
    {
        get
        {
            return _properties;
        }
    }

    void Awake () {
        _rectTransform = transform as RectTransform;
        _image = GetComponent<Image>();
        Properties.PropertyUpdated = () => {
            switch (_properties.Type)
            {
                //case 0: _image.color = Color.white; break;
                case 5: _image.color = Color.magenta; break;
                case 1: _image.color = Color.red; ; break;
                case 2: _image.color = Color.green; ; break;
                case 3: _image.color = Color.blue; ; break;
                case 4: _image.color = Color.yellow; ; break;
                default:
                    break;
            }
            _defaultColor = _image.color;
            _defaultColorEmpty = new Color(_defaultColor.r, _defaultColor.g, _defaultColor.b, 0);
            name = "[" + _properties.Address.X + "," + _properties.Address.Y + "] : " + _properties.Type;
        };
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //OnSelectAction?.Invoke(this);
        //Debug.Log(Address +" ; "+Type);
    }

}
