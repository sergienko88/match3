﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputHelper {

    public static Vector2 PointerPosition {
        get {
#if UNITY_EDITOR
            return Input.mousePosition;
#endif

#if !UNITY_EDITOR
            return Input.touches.Length>0? Input.touches[0].position:Vector2.zero;
#endif
        }
    }

}
