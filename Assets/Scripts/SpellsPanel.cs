﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpellsPanel : UIRectTransform {

    List<UISpell> _uiSpells = new List<UISpell>();

    UISpell _currentSelectedUISpell = null;

    public UISpell CurrentSelectedUISpell
    {
        get
        {
            return _currentSelectedUISpell;
        }
    }

    Vector2 _offset = new Vector2(10,0);

    UISpell GetInitializedSpell(Spell spell)
    {
        UISpell uiSpell = Instantiate(Resources.Load<UISpell>("Prefabs/UISpell"));
        uiSpell.Spell = spell;
        return uiSpell;
    }

    void Initialize()
    {
        UISpell t_uispell = null;
        for (int i = 0; i < PlayerInfo.Spells.Count; i++)
        {
            t_uispell = GetInitializedSpell(PlayerInfo.Spells[i]);
            t_uispell.RectTransform.SetParent(RectTransform);
            t_uispell.RectTransform.anchoredPosition =
                (Vector2.zero  + Vector2.right *(t_uispell.RectTransform.sizeDelta.x/2+ _offset.x)) + Vector2.right*i*(t_uispell.RectTransform.sizeDelta.x+_offset.x)
                -Vector2.right*(PlayerInfo.Spells.Count* (t_uispell.RectTransform.sizeDelta.x /2+_offset.x)- _offset.x/2)
                ;
            //t_uispell.OnPointerDownAction = UISpellPointerDown;
            _uiSpells.Add(t_uispell);
        }
    }

    void UISpellPointerDown(UISpell selectedUISpell)
    {        
        _currentSelectedUISpell = selectedUISpell;
        foreach (var item in _uiSpells)
        {
            if (item != selectedUISpell)
            {
                item.Deselect();
            }
            else
            {
                item.Select();
            }
        }
    }

    void Awake()
    {
        Initialize();
        InputHandler.OnTouchDown += () => {
            #region currentSpell not null
            if (_currentSelectedUISpell)
            {  
                List<RaycastResult> castResult = InputHandler.Raycast();
                if (castResult.Count > 0)
                {
                    UICell castedUICell = castResult[0].gameObject.GetComponent<UICell>();
                    if (castedUICell)
                    {
                        _currentSelectedUISpell.TryUseSpell(castedUICell);
                        return;
                    }
                    UISpell  castedUISpell = castResult[0].gameObject.GetComponent<UISpell>();
                    if (castedUISpell)
                    {                        
                        if (_currentSelectedUISpell == castedUISpell)
                        {                     
                            _currentSelectedUISpell = null;
                            foreach (var item in _uiSpells)
                            {
                                item.Deselect();
                            }
                        }
                        else
                        {                            
                            UISpellPointerDown(castedUISpell);
                        }
                    }
                }
                else
                {                    
                    _currentSelectedUISpell = null;
                    foreach (var item in _uiSpells)
                    {
                        item.Deselect();                        
                    }
                }
            }
            #endregion
            else
            {
                List<RaycastResult> castResult = InputHandler.Raycast();               
                if (castResult.Count > 0)
                {
                    UISpell castedUISpell = castResult[0].gameObject.GetComponent<UISpell>();
                    if (castedUISpell)
                    {
                        if (_currentSelectedUISpell == castedUISpell)
                        {
                            _currentSelectedUISpell = null;
                            foreach (var item in _uiSpells)
                            {
                                item.Deselect();
                            }
                        }
                        else
                        {
                            UISpellPointerDown(castedUISpell);
                        }
                    }
                }                
            }
        };
    }
}
