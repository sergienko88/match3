﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class UISpell : UIRectTransform, IPointerDownHandler{

    Spell _spell;

    public Spell Spell
    {
        get
        {
            return _spell;
        }

        set
        {
            _spell = value;
            UpdateSpellUI(value);
        }
    }

    Image _background;

    [SerializeField]
    Image _spellImage;

    [SerializeField]
    Text _spellName;

    [SerializeField]
    Text _spellCount;

    public Action <UISpell> OnPointerDownAction;

    void UpdateSpellUI(Spell spellValue)
    {
        _spellName.text = spellValue.Name;
        _spellCount.text = spellValue.Count.ToString();
        name = spellValue.Name;
    }

    // Use this for initialization
    void Awake () {
        _background = transform.GetComponent<Image>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {       
        OnPointerDownAction?.Invoke(this);
    }

    public void Select()
    {
        _background.color = Color.green;
    }

    public void Deselect()
    {
        _background.color = Color.white;
    }

    public bool TryUseSpell(UICell uicell)
    {        
        return false;
    }

}
