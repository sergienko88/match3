﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIField : UIRectTransform {
    [SerializeField]
    UICell _uiCellPrefab;

    UICell _templatePrefab;

    [SerializeField]
    Image _selector;

    [SerializeField]
    UICell[,] _allUIcells = new UICell[0, 0];

    Vector2 _offset = new Vector2(3,3);    
    UICell _currentSelectedCell;
    int MinCelltoMatch = 3;
    Vector2 _cellSizeOnScreen = Vector2.zero;
    bool _isPointerDown = false;
    bool _isAnimate = false;
    Vector2 _startPointPosition = new Vector2();
    bool _isChangeCells = false;
    List<UICell> empty = new List<UICell>();
    int _cellsFinished = 0;
    SwipeState _swipe = SwipeState.None;
    int _animationsFallingFinishedCount = 0;
    List<IEnumerator> _cellsAnimationsFalling = new List<IEnumerator>();

    Image _fieldBackground;

    int _score = 0;
    public  Action<int> ChangeScore;
    public int Score
    {
        get
        {
            return _score;
        }

        private set
        {
            _score = value;
            ChangeScore?.Invoke(_score);
        }
    }

    public Action OnTurnFinish;

    Properties[,] _allCellProperties
    {
        get
        {
            Properties[,] fs = new Properties[_allUIcells.GetLength(0), _allUIcells.GetLength(1)];

            for (int i = 0; i < _allUIcells.GetLength(0); i++)
            {
                for (int j = 0; j < _allUIcells.GetLength(1); j++)
                {
                    fs[i, j] = _allUIcells[i, j].Properties;
                }
            }
            return fs;
        }
    }

    void SetCellSizeOnScreen()
    {
        Vector3[] corners = new Vector3[4];
        _uiCellPrefab.GetComponent<RectTransform>().GetWorldCorners(corners);
        //for (int i = 0; i < corners.Length; i++)
        //{
        //    Debug.Log(corners[i]);
        //    GameObject s = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //    s.gameObject.transform.position = corners[i];
        //    s.name = corners[i].ToString();
        //}
        _cellSizeOnScreen = new Vector2(Mathf.Abs(corners[3].x - corners[0].x), Mathf.Abs(corners[1].y - corners[0].y));
    }

    void InitializeCellSelector()
    {
        _selector.rectTransform.sizeDelta = _templatePrefab.RectTransform.sizeDelta * 1.1f;
        _selector.gameObject.SetActive(false);
    }

    void Awake() {
        _fieldBackground = GetComponent<Image>();
        _rectTransform = transform as RectTransform;
        _templatePrefab = (Instantiate(_uiCellPrefab));
        _templatePrefab.transform.SetParent(transform);
        _templatePrefab.gameObject.SetActive(false);
        SetCellSizeOnScreen();
    }

    void Start()
    {
        InputHandler.OnTouchDown += OnTouchDown;
        InputHandler.OnTouchUp += OnTouchUp;
        InitializeCellSelector();
        Create(9,9);        
        TryFindMatch();
    }

    void OnTouchDown()
    {
        if (!_isAnimate && !_isChangeCells)
        {
            OnPointerDown();
        }
    }

    void OnTouchUp()
    {
        if (!_isAnimate && !_isChangeCells)
        {
            OnPointerUp();
        }
    }

    void TryFindMatch()
    {
        _isAnimate = true;
        empty = new List<UICell>();
        Properties[,] fs = new Properties[_allUIcells.GetLength(0), _allUIcells.GetLength(1)];

        for (int i = 0; i < _allUIcells.GetLength(0); i++)
        {
            for (int j = 0; j < _allUIcells.GetLength(1); j++)
            {
                fs[i, j] = _allUIcells[i, j].Properties;
            }
        }
        var matches = FindMatch(fs);

        if (matches.Count > 0)
        {
            matches.Distinct();
            foreach (var item in matches)
            {                
                _allUIcells[item.Address.X, item.Address.Y].Properties.IsEmpty = true;
                empty.Add(_allUIcells[item.Address.X, item.Address.Y]);
            }
            CalculateCellsFalling();
        }
        else
        {            
            _isAnimate = false;
            OnTurnFinish?.Invoke();
        }
    }
    
    void CalculateCellsFalling()
    {   
        UICell t_uicell = null;        
        for (int col = 0; col < _allUIcells.GetLength(0); col++)
        {
            int emptyCell = 1;
            for (int row = _allUIcells.GetLength(1) - 1; row >= 0; row--)
            {  
                while (_allUIcells[col, row].Properties.IsEmpty)
                {   
                    for (int h = row; h > 0; h--)
                    {   
                        _allUIcells[col, h] = _allUIcells[col, h - 1];
                        _allUIcells[col, h].Properties.Address = new DimentionAddress2 { X = col, Y = h };
                    }

                    //создаем новую ячейку
                    {
                        t_uicell = null;
                        t_uicell = CreateCell();
                        var address = new DimentionAddress2
                        {
                            X = col,
                            Y = 0
                        };
                        t_uicell.Properties.Type = UnityEngine.Random.Range(1, 5);
                        t_uicell.Properties.Address = address;
                        _allUIcells[address.X, address.Y] = t_uicell;
                        t_uicell.RectTransform.anchoredPosition = new Vector2(
                            (t_uicell.RectTransform.rect.width / 2 + _offset.x) + t_uicell.Properties.Address.X * (t_uicell.RectTransform.rect.width + _offset.x),
                            -(t_uicell.RectTransform.rect.height / 2 + _offset.y)  - (t_uicell.Properties.Address.Y- emptyCell) * (t_uicell.RectTransform.rect.height + _offset.y)
                        );
                    }
                    emptyCell++;
                }                
            }
        }
        
        StartCoroutine(ShowMatches(()=> {
            Score += empty.Sum(e => e.Properties.Points);
            empty.ForEach(e => Destroy(e.gameObject));
            empty = new List<UICell>();
            StartFalling();
        }));        
    }

    IEnumerator ShowMatches(Action onFinish)
    {
        yield return new WaitForSeconds(.1f);
        float timer = 0;
        float duration = .2f;
        while (timer < duration) {
            timer += Time.deltaTime;
            empty.ForEach(e => {
                e.Image.color = Color.Lerp(e.DefaultColor, e.DefaultColorEmpty, timer / duration);
            });
            yield return null;
        }                
        onFinish?.Invoke();
    }

    void StartFalling()
    {
        for (int i = 0; i < _allUIcells.GetLength(0); i++)
        {
            for (int j = 0; j < _allUIcells.GetLength(1); j++)
            {
                _cellsAnimationsFalling.Add(
                AnimateCell(_allUIcells[i,j], _allUIcells[i, j].RectTransform.anchoredPosition, GetCorrectPositionForCell(_allUIcells[i, j]),
                ()=>
                {
                    _animationsFallingFinishedCount++;
                    CheckForFinishAllCellsFallingAnimations();
                }));
            }
        }
        _animationsFallingFinishedCount = 0;
        _cellsAnimationsFalling.ForEach(a => StartCoroutine(a));
    }

    Vector2 GetCorrectPositionForCell( UICell uiCell)
    {
        return new Vector2(
            (uiCell.RectTransform.rect.width / 2 + _offset.x) + uiCell.Properties.Address.X * (uiCell.RectTransform.rect.width + _offset.x),
            -(uiCell.RectTransform.rect.height / 2 + _offset.y) - uiCell.Properties.Address.Y * (uiCell.RectTransform.rect.height + _offset.y)
        );
    }

    UICell CreateCell()
    {
        UICell t_uicell;
        t_uicell = Instantiate(_templatePrefab);
        t_uicell.gameObject.SetActive(true);
        t_uicell.RectTransform.SetParent(_rectTransform);
        t_uicell.RectTransform.localScale = Vector2.one;
        return t_uicell;
    }
    
    void Create(int width, int height)
    {
        int[,] field = new int[width, height];
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                field[i,j] = UnityEngine.Random.Range(1, 5);
            }
        }
        Create(field);
    }

    void Create(int[,] fieldTypes)
    {

        int width = fieldTypes.GetLength(1);
        int height = fieldTypes.GetLength(0);
        _allUIcells = new UICell[width, height];
        UICell t_uicell;
        int type = 0;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                t_uicell = CreateCell();
                _allUIcells[j, i] = t_uicell;
                t_uicell.Properties.Address = new DimentionAddress2 { X = j, Y = i };
                t_uicell.RectTransform.anchoredPosition = GetCorrectPositionForCell(t_uicell);

                type = fieldTypes[i, j];
                t_uicell.Properties.Type = type;
            }
        }

        _fieldBackground.rectTransform.sizeDelta = new Vector2(
          width * (_templatePrefab.RectTransform.rect.width + _offset.x) + _offset.x,
          height * (_templatePrefab.RectTransform.rect.height + _offset.y) + _offset.y
       );

        List<Properties> matches = FindMatch(_allCellProperties);
        while (matches.Count > 0)
        {
            for (int i = 0; i < matches.Count; i++)
            {
                matches[i].Type = UnityEngine.Random.Range(1, 5);
            }

            matches = FindMatch(_allCellProperties);
        }
    }

    List<Properties> FindMatch(Properties[,] fieldCellsProperties)
    {
        List<Properties> matches = new List<Properties>();
        List<Properties> dementionMatches = new List<Properties>();
        List<Properties> temp_matches = new List<Properties>();
        //горизонтальные
      
        #region horiz
        for (int row = 0; row < fieldCellsProperties.GetLength(1); row++)
        {
            if (temp_matches.Count >= MinCelltoMatch)
            {
                dementionMatches.AddRange(temp_matches);
            }
            temp_matches = new List<Properties>();
            temp_matches.Add(fieldCellsProperties[0, row]);

            for (int col = 0; col < fieldCellsProperties.GetLength(0) -1; col++)
            {
              
                if (fieldCellsProperties[col, row].Type == fieldCellsProperties[col + 1, row].Type)
                {
                    temp_matches.Add(fieldCellsProperties[col + 1, row]);
                }
                else
                {
                    if (temp_matches.Count >= MinCelltoMatch)
                    {
                        dementionMatches.AddRange(temp_matches);
                    }
                    temp_matches = new List<Properties>();
                    temp_matches.Add(fieldCellsProperties[col + 1, row]);                    
                }

                if(col+1 == fieldCellsProperties.GetLength(0) - 1)
                {
                    if (temp_matches.Count >= MinCelltoMatch)
                    {
                        dementionMatches.AddRange(temp_matches);
                    }
                    temp_matches = new List<Properties>();
                }
            }
        }
        matches.AddRange(dementionMatches);

        //dementionMatches.ForEach(m =>
        //{
        //    var mh = Instantiate(_uiCellPrefab);
        //    mh.gameObject.transform.SetParent(_allcells[m.Address.X,m.Address.Y].RectTransform);
        //    mh.RectTransform.localScale = Vector2.one;
        //    mh.RectTransform.localPosition = Vector2.zero;
        //    mh.RectTransform.sizeDelta = _allcells[m.Address.X, m.Address.Y].RectTransform.sizeDelta / 2;
        //    mh.Image.color = Color.black;
        //});

        #endregion
      
        //вертикальные
     
        #region vert
        temp_matches = new List<Properties>();
        dementionMatches = new List<Properties>();

        for (int col = 0; col < fieldCellsProperties.GetLength(0); col++)
        {

            if (temp_matches.Count >= MinCelltoMatch)
            {
                dementionMatches.AddRange(temp_matches);
            }
            temp_matches = new List<Properties>();
            temp_matches.Add(fieldCellsProperties[col,0]);

            for (int row = 0; row < fieldCellsProperties.GetLength(1) - 1; row++)
            {
                if (fieldCellsProperties[col, row].Type == fieldCellsProperties[col, row + 1].Type)
                {
                    temp_matches.Add(fieldCellsProperties[col, row + 1]);
                }
                else
                {   
                    if (temp_matches.Count >= MinCelltoMatch)
                    {
                        dementionMatches.AddRange(temp_matches);                        
                    }
                    temp_matches = new List<Properties>();
                    temp_matches.Add(fieldCellsProperties[col, row + 1]);
                }

                if (row + 1 == fieldCellsProperties.GetLength(1) - 1)
                {
                    if (temp_matches.Count >= MinCelltoMatch)
                    {
                        dementionMatches.AddRange(temp_matches);
                    }
                }
            }
        }
        matches.AddRange(dementionMatches);
        //dementionMatches.ForEach(m =>
        //{
        //    var mh = Instantiate(_uiCellPrefab);
        //    mh.gameObject.transform.SetParent(_allcells[m.Address.X, m.Address.Y].RectTransform);
        //    mh.RectTransform.localScale = Vector2.one;
        //    mh.RectTransform.localPosition = Vector2.zero;
        //    mh.RectTransform.sizeDelta = _allcells[m.Address.X, m.Address.Y].RectTransform.sizeDelta / 3;
        //    mh.Image.color = Color.gray;
        //});
        #endregion
      
        return matches;
    }  

    void TryChangeCells(UICell a,UICell b)
    {
        _currentSelectedCell = null;
        _cellsFinished = 0;
        Vector2 pos_a = new Vector2(a.RectTransform.anchoredPosition.x, a.RectTransform.anchoredPosition.y);
        Vector2 pos_b = new Vector2(b.RectTransform.anchoredPosition.x,b.RectTransform.anchoredPosition.y);

        StartCoroutine(AnimateCell(a,pos_a, pos_b, () => {
            _cellsFinished++;
            OnAnimateChangeCell(a,b);
        }));


        StartCoroutine(AnimateCell(b, pos_b, pos_a, () =>
        {
            _cellsFinished++;
            OnAnimateChangeCell(a,b);
        }));

    }

    void OnAnimateChangeCell(UICell a,UICell b)
    {
        if (_cellsFinished == 2)
        {
            _isChangeCells = false;
            
            Properties[,] nextFieldState = new Properties[_allUIcells.GetLength(0), _allUIcells.GetLength(1)];
            for (int i = 0; i < _allUIcells.GetLength(0); i++)
            {
                for (int j = 0; j < _allUIcells.GetLength(1); j++)
                {
                    nextFieldState[i, j] = new Properties
                    {
                        Address = new DimentionAddress2 { X = _allUIcells[i, j].Properties.Address.X, Y = _allUIcells[i, j].Properties.Address.Y },
                        @Type = _allUIcells[i, j].Properties.Type
                    };
                }
            }
            nextFieldState[a.Properties.Address.X, a.Properties.Address.Y].Type = b.Properties.Type;
            nextFieldState[b.Properties.Address.X, b.Properties.Address.Y].Type = a.Properties.Type;
           
            var matches = FindMatch(nextFieldState);
            if (matches.Count > 0)
            {
                DimentionAddress2 saved_a = new DimentionAddress2 { X = a.Properties.Address.X, Y = a.Properties.Address.Y };
                DimentionAddress2 saved_b = new DimentionAddress2 { X = b.Properties.Address.X, Y = b.Properties.Address.Y };
                _allUIcells[a.Properties.Address.X, a.Properties.Address.Y] = b;
                _allUIcells[b.Properties.Address.X, b.Properties.Address.Y] = a;

                a.Properties.Address = saved_b;
                b.Properties.Address = saved_a;
                TryFindMatch();
            }
            else
            {
                _isChangeCells = true;
                _cellsFinished = 0;
                Vector2 pos_a = new Vector2(a.RectTransform.anchoredPosition.x, a.RectTransform.anchoredPosition.y);
                Vector2 pos_b = new Vector2(b.RectTransform.anchoredPosition.x, b.RectTransform.anchoredPosition.y);

                StartCoroutine(AnimateCell(a, pos_a, pos_b, () => {
                    _cellsFinished++;
                    OnAnimateCellChangeNotMatches(a,b);
                }));


                StartCoroutine(AnimateCell(b, pos_b, pos_a, () =>
                {
                    _cellsFinished++;
                    OnAnimateCellChangeNotMatches(a,b);
                }));
            }
        }
    }

    void OnAnimateCellChangeNotMatches(UICell a,UICell b)
    {
        if (_cellsFinished == 2)
        {
            _isAnimate = false;
            _isChangeCells = false;
        }
    }

    enum SwipeState {
        Right,
        Left,
        Up,
        Down,
        None,
        Stationary            
    }

    SwipeState GetSwipe(Vector2 starPosition,Vector2 endPosition)
    {
        Vector2 swipe = starPosition - endPosition;
        if (swipe.magnitude > _cellSizeOnScreen.x*.3f)
        {
            swipe.Normalize();

            if (swipe.x > .5f && Mathf.Abs(swipe.y) < .5f)
            {
                return SwipeState.Left;
            }
            if (swipe.x < -.5f && Mathf.Abs(swipe.y) < .5f)
            {
                return SwipeState.Right;
            }
            if (swipe.y > .5f && Mathf.Abs(swipe.x) < .5f)
            {
                return SwipeState.Down;
            }

            if (swipe.y < -.5f && Mathf.Abs(swipe.x) < .5f)
            {
                return SwipeState.Up;
            }
        }
        return SwipeState.Stationary;
    }

    UICell PointerRaycast(Vector2 position)
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        pointerData.position = position;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        UICell uicell = null;
        if (results.Count > 0)
        {
            uicell = results[0].gameObject.GetComponent<UICell>();
        }
        return uicell;
    }

    void Restart()
    {
        if (_isAnimate) return;
        for (int i = 0; i < _allUIcells.GetLength(0); i++)
        {
            for (int j = 0; j < _allUIcells.GetLength(1); j++)
            {
                if (_allUIcells[i, j] && _allUIcells[i, j].gameObject)
                {
                    Destroy(_allUIcells[i, j].gameObject);
                }
            }
        }

        _allUIcells = new UICell[0, 0];

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        Start();
    }

    IEnumerator AnimateCell(UICell cell,Vector2 startPosition, Vector2 endPosition, Action onFinish)
    {        
        if (cell.RectTransform.anchoredPosition == endPosition)
        {
            onFinish?.Invoke();
            yield break;
        }

        Vector2 allLength = (endPosition - startPosition);

        float acceleration = 1f;        
        while (
            Vector3.Distance(startPosition, cell.RectTransform.anchoredPosition)<
            Vector3.Distance(endPosition, startPosition)
        )
        {         
            acceleration += 10f * Time.deltaTime;
            cell.RectTransform.anchoredPosition += allLength.normalized * 200f * acceleration * Time.deltaTime;
            if (Vector3.Distance(startPosition, cell.RectTransform.anchoredPosition)>
            Vector3.Distance(endPosition, startPosition))
            {
                cell.RectTransform.anchoredPosition = endPosition;
            }
            yield return null;
        }       
        
        onFinish?.Invoke();
    }

    void CheckForFinishAllCellsFallingAnimations()
    {        
        if (_animationsFallingFinishedCount>= _cellsAnimationsFalling.Count)
        {            
            _cellsAnimationsFalling = new List<IEnumerator>();
            TryFindMatch();
        }
    }

    void Update()
    {
        OnPointerTouching(); 
    }
    
    void OnPointerTouching()
    {        
        if (!_isChangeCells &&
            Vector2.Distance(_startPointPosition, InputHelper.PointerPosition)>_cellSizeOnScreen.x 
            && _currentSelectedCell 
            &&  _isPointerDown
            && !_isAnimate
        )
        {                
            _swipe = GetSwipe(_startPointPosition, InputHelper.PointerPosition);
            if (_swipe != SwipeState.Stationary)
            {
                UICell next_cell = null;
                DimentionAddress2 nextAddress = new DimentionAddress2 {
                    X = _currentSelectedCell.Properties.Address.X,
                    Y = _currentSelectedCell.Properties.Address.Y

                };
                switch (_swipe)
                {
                    case SwipeState.Right:
                        nextAddress.X++;
                        break;
                    case SwipeState.Left:
                        nextAddress.X--;
                        break;
                    case SwipeState.Up:
                        nextAddress.Y--;
                        break;
                    case SwipeState.Down:
                        nextAddress.Y++;
                        break;

                    default:
                        break;
                }


                if (nextAddress.X >= 0 && nextAddress.X < _allUIcells.GetLength(0)
                    && nextAddress.Y >= 0 && nextAddress.Y < _allUIcells.GetLength(1)
                )
                {

                    next_cell = _allUIcells[nextAddress.X, nextAddress.Y];                        
                    if (next_cell != null && _currentSelectedCell && _currentSelectedCell != next_cell)
                    {
                        if (!_isChangeCells)
                        {
                            _isChangeCells = true;
                            _selector.gameObject.SetActive(false);
                            _isPointerDown = false;
                            TryChangeCells(_currentSelectedCell, next_cell);
                        }
                    }
                }

            }
        }        
    }
    
    bool isCanReplace(UICell a,UICell b)
    {
        return Vector2.Distance(
                a.RectTransform.anchoredPosition,
                b.RectTransform.anchoredPosition
            )
            <= (_templatePrefab.RectTransform.rect.width + _offset.x);
    }

    void OnPointerUp()
    {
        _isPointerDown = false;
    }

    void OnPointerDown()
    {
        _isPointerDown = true;
        _startPointPosition = InputHelper.PointerPosition;
        UICell t_selected = PointerRaycast(_startPointPosition);
        if (t_selected)
        {
            if (_currentSelectedCell)
            {
                if (t_selected != _currentSelectedCell)
                {                    
                    if (isCanReplace(t_selected, _currentSelectedCell))
                    {                        
                        _selector.gameObject.SetActive(false);
                        TryChangeCells(_currentSelectedCell, t_selected);
                    }
                    else
                    {   
                        _currentSelectedCell = t_selected;
                        _selector.gameObject.SetActive(true);
                        _selector.rectTransform.anchoredPosition = t_selected.RectTransform.anchoredPosition;
                    }
                }
                else
                {                    
                    _currentSelectedCell = t_selected;
                    _selector.gameObject.SetActive(true);
                    _selector.rectTransform.anchoredPosition = t_selected.RectTransform.anchoredPosition;
                }
            }
            else
            {                
                _currentSelectedCell = t_selected;
                _selector.gameObject.SetActive(true);
                _selector.rectTransform.anchoredPosition = t_selected.RectTransform.anchoredPosition;
            }
        }
        else
        {            
            _selector.gameObject.SetActive(false);
            _currentSelectedCell = null;
        }


    }
}
