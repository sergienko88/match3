﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScorePanel : MonoBehaviour {
    [SerializeField]
    UIField _field;

    [SerializeField]
    Text _scoreValue;

    void SetValue(int value)
    {        
        if (_scoreValue)
        {            
            _scoreValue.text = value.ToString();
        }
    }

	// Use this for initialization
	void Start () {
        if (_field)
        {
            _field.ChangeScore += SetValue;
        }
    }
}
