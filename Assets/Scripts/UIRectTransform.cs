﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRectTransform : MonoBehaviour {

    protected RectTransform _rectTransform;

    public RectTransform RectTransform
    {
        get
        {
            if (!_rectTransform)
            {
                _rectTransform = transform as RectTransform;
            }
            return _rectTransform;
        }
    }
}
