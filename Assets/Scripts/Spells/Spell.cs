﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell  {
    public int Id
    {
        get
        {
            return _id;
        }

        set
        {
            _id = value;
        }
    }
    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }
    public int Count
    {
        get
        {
            return _count;
        }

        set
        {
            _count = value;
        }
    }

    protected int _id = 0;
    protected string _name = "DefaultSpell";
    protected int _count = 0;

    public bool TryUse()
    {
        if (_count <= 0) return false;

        bool state = false;        

        if (state)
        {
            Use();
        }
        else
        {

        }
        return state;
    }

    public void Use()
    {
        _count--;
        if (_count < 0)
        {
            _count = 0;
        }
    }
}
