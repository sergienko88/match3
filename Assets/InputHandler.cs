﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputHandler : MonoBehaviour {

    public static Action OnTouchDown;
    public static Action OnTouchUp;
    public static List<RaycastResult> Raycast()
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        pointerData.position = InputHelper.PointerPosition;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);
        return results;
    }

    public static InputHandler INSTANCE = null;

    void Awake()
    {
        INSTANCE = this;
    }
    
    // Update is called once per frame
    void Update () {
       
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {            
            OnTouchDown?.Invoke();
        }
        if (Input.GetMouseButtonUp(0))
        {
            OnTouchUp?.Invoke();
        }
#elif !UNITY_EDITOR
        if (Input.touches.Length > 0)
        {
            switch (Input.touches[0].phase)
            {
                case TouchPhase.Began:
                    OnTouchDown?.Invoke();
                    break;
                case TouchPhase.Moved:
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Ended:
                    OnTouchUp?.Invoke();
                    break;
                case TouchPhase.Canceled:
                    OnTouchUp?.Invoke();
                    break;
                default:
                    break;
            }
        }
#endif
    }
}
